package com.developer.nestor.airepurisimo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;
import com.developer.nestor.airepurisimo.Modelo.HttpHandler;
import com.developer.nestor.airepurisimo.Modelo.MedicionContaminacion;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class AirePurisimo extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.OnConnectionFailedListener, GoogleApiClient.ConnectionCallbacks {
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 1;
    private static final int MY_PERMISSIONS_REQUEST_COARSE_LOCATION = 2;
    private static final int PETICION_CONFIG_UBICACION = 3;
    private String TAG = AirePurisimo.class.getSimpleName(); // Para identificar los errores en el LOG

    private List<MedicionContaminacion> medicionList;
    private Type medicionListType;
    private ArrayList<LatLng> localizacionDibujadas;

    /* Interesante leer:
     * 1. http://www.sgoliver.net/blog/mapas-en-android-google-maps-android-api-1/
     * 2. http://www.sgoliver.net/blog/mapas-en-android-google-maps-android-api-2/
     * 3. http://www.sgoliver.net/blog/mapas-en-android-google-maps-android-api-3/
     * 4. http://www.sgoliver.net/blog/localizacion-geografica-en-android-1/ (localizacion)
     * 5. http://www.sgoliver.net/blog/localizacion-geografica-en-android-2/
     * 6. https://developers.google.com/maps/documentation/android-sdk/marker Personalizar marcadores
     */

    private GoogleMap mMap;
    private GoogleApiClient apiClient;
    private FusedLocationProviderClient mFusedLocationClient;
    Location lastLocation;
    private LocationRequest locRequest;
    private LocationCallback mLocationCallback;
    private boolean isLocationUpdatesActivated = false;

    private ProgressDialog pDialog;
    Marker ultimoMarker;
    AsyncTaskGetMedicionesCo2 jsonTask;
    Timer timer;
    TimerTask task;

    // URL to get contacts JSON
    // private static String url = "http://api.androidhive.info/contacts/";
    // URL wifi de casa
    //private static String url = "http://192.168.1.36/ecoterrax/modelo/obtenerDetalleHuerto.php?idHuerto=2";

    // URL wifi de Colegio La Purísima Valencia
    private static final String url="http://192.168.1.187:3000/contaminacion";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_aire_purisimo);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        apiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
        // Para obtener la última localización conocida
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }
                for (Location location : locationResult.getLocations()) {
                    // Update UI with location data
                    updateLocationMaps(location);
                }
            }
        };
        localizacionDibujadas = new ArrayList<>();
        enableLocationUpdates();
        // insertarMarcador(); // Aquí ésto no funciona porque mMap tendrá el mapa en el callback.
        setRepeatingAsyncTask();
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) { // Se llama cuando está disponible nuestro mapa
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        // insertarMarcador(); // En este punto el mapa ya existe, puedo poner marcadores.
        }

    private void updateLocationMaps(Location loc) {
        if (loc != null && mMap != null) {

            LatLng myPosition = new LatLng(loc.getLatitude(), loc.getLongitude());
            if (ultimoMarker!=null){ // Elimino el marker de la última posición localizada, sino se llenaría de markers.
                ultimoMarker.remove();
            }
            ultimoMarker = mMap.addMarker(new MarkerOptions().position(myPosition)
                    .title("Estoy aquí"));
            // mMap.moveCamera(CameraUpdateFactory.newLatLng(myPosition));
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        // Interfaz GoogleApiClient.OnConnectionFailedListener
        //Se ha producido un error que no se puede resolver automáticamente
        //y la conexión con los Google Play Services no se ha establecido.
        Log.e("ERROR_LOCATION", "Error grave al conectar con Google Play Services");
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Interfaz GoogleApiClient.ConnectionCallbacks
        //Conectado correctamente a Google Play Services

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            // NO HARIA FALTA PEDIRLO PORQUE ES DE LOCALIZACIÓN TAMBIÉN, Y CON EL DE ANTES SOBRA.
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST_COARSE_LOCATION);

        } else { // Permisos concedidos ya anteriormente
            //Location lastLocation = LocationServices.FusedLocationApi.getLastLocation(apiClient);

            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations, this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                lastLocation = location;
                            }
                        }
                    });

            updateLocationMaps(lastLocation);
        }
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // coarse_location-related task you need to do.
                    mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations, this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        lastLocation = location;
                                    }
                                }
                            });

                    updateLocationMaps(lastLocation);

                } else {
                    Log.e("ERROR", "Permiso FINE_LOCATION denegado");
                    // this.finish(); // Salgo de la app
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;
            // NO HARIA FALTA PEDIRLO PORQUE ES DE LOCALIZACIÓN TAMBIÉN, Y CON EL DE ANTES SOBRA.
            case MY_PERMISSIONS_REQUEST_COARSE_LOCATION:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // coarse_location-related task you need to do.
                    mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // Got last known location. In some rare situations, this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        lastLocation = location;
                                    }
                                }
                            });

                    updateLocationMaps(lastLocation);
                } else {
                    Log.e("ERROR", "Permiso COARSE_LOCATION denegado");
                    // this.finish(); // Salgo de la app
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                break;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        // Interfaz GoogleApiClient.ConnectionCallbacks

    }

    private void enableLocationUpdates() {
        // Método para mantener la localización actualizada.
        locRequest = new LocationRequest();
        locRequest.setInterval(2000);
        locRequest.setFastestInterval(1000);
        locRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locRequest);

        Task<LocationSettingsResponse> result =
                LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());

        result.addOnCompleteListener(new OnCompleteListener<LocationSettingsResponse>() {
            @Override
            public void onComplete(Task<LocationSettingsResponse> task) {
                try {
                    LocationSettingsResponse response = task.getResult(ApiException.class);
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    Log.i("OK", "Configuración correcta");
                    startLocationUpdates();


                } catch (ApiException exception) {
                    switch (exception.getStatusCode()) {
                        case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                            // Location settings are not satisfied. But could be fixed by showing the
                            // user a dialog.
                            try {
                                // Cast to a resolvable exception.
                                ResolvableApiException resolvable = (ResolvableApiException) exception;
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                resolvable.startResolutionForResult(
                                        AirePurisimo.this,
                                        PETICION_CONFIG_UBICACION);
                            } catch (IntentSender.SendIntentException e) {
                                // Ignore the error.
                            } catch (ClassCastException e) {
                                // Ignore, should be an impossible error.
                            }
                            break;
                        case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                            // Location settings are not satisfied. However, we have no way to fix the
                            // settings so we won't show the dialog.
                            Log.i("OK", "No se puede cumplir la configuración de ubicación necesaria");
                            break;
                    }
                }
            }
        });
        /* DEPRECATED
        LocationSettingsRequest locSettingsRequest =
                new LocationSettingsRequest.Builder()
                        .addLocationRequest(locRequest)
                        .build();

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(
                        apiClient, locSettingsRequest);

        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:

                        Log.i(LOGTAG, "Configuración correcta");
                        startLocationUpdates();
                        break;

                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            Log.i(LOGTAG, "Se requiere actuación del usuario");
                            status.startResolutionForResult(MainActivity.this, PETICION_CONFIG_UBICACION);
                        } catch (IntentSender.SendIntentException e) {
                            btnActualizar.setChecked(false);
                            Log.i(LOGTAG, "Error al intentar solucionar configuración de ubicación");
                        }
                        break;

                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.i(LOGTAG, "No se puede cumplir la configuración de ubicación necesaria");
                        btnActualizar.setChecked(false);
                        break;
                }
            }
        });

        */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case PETICION_CONFIG_UBICACION:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        startLocationUpdates();
                        break;
                    case Activity.RESULT_CANCELED:
                        Log.i("ERROR", "El usuario no ha realizado los cambios de configuración necesarios");
                        // this.finish();
                        break;
                }
                break;
        }
    }

    private void startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            //Ojo: estamos suponiendo que ya tenemos concedido el permiso.
            //Sería recomendable implementar la posible petición en caso de no tenerlo.

            Log.i("OK", "Inicio de recepción de ubicaciones");

            // LocationServices.FusedLocationApi.requestLocationUpdates(apiClient, locRequest, MainActivity.this); // DEPRECATED
            // Nuevo: https://developer.android.com/training/location/receive-location-updates#java
            mFusedLocationClient.requestLocationUpdates(locRequest, mLocationCallback, null);
            isLocationUpdatesActivated = true;
        }
    }

    private void stopLocationUpdates() {
        mFusedLocationClient.removeLocationUpdates(mLocationCallback);
        isLocationUpdatesActivated = false;
    }

    private void insertarMarcador() {
        if(mMap!=null) {
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(39.46448729, -0.40475909))
                    .title("Colegio La Purisima Valencia"));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        stopLocationUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!isLocationUpdatesActivated) {
            startLocationUpdates();
        }
    }

    private void setRepeatingAsyncTask() {

        final Handler handler = new Handler();
        timer = new Timer();

        task = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            jsonTask = new AsyncTaskGetMedicionesCo2();
                            jsonTask.execute();
                        } catch (Exception e) {
                            // error, do something
                            Toast.makeText(getApplicationContext(), "ERROR al conectar con el servidor: "+e.toString(),Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        };
        // Actualizamos cada minuto ya que el tiempo mínimo de refresco del script en python es cada 60 segundos
        timer.schedule(task, 0, 60*1000);  // interval of 60 seconds

    }

    /**
     * Async task class to get json by making HTTP call
     */
    private class AsyncTaskGetMedicionesCo2 extends AsyncTask<Void, Void, Void> {

        String id;
        String fecha;
        String hora;
        int ppm;
        double latitud;
        double longitud;
        float temperatura;
        int humedad;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

            pDialog = new ProgressDialog(AirePurisimo.this);
            pDialog.setMessage("Espere por favor...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            HttpHandler sh = new HttpHandler();

            // Making a request to url and getting response
            String jsonStr = sh.makeServiceCall(url);

            Log.e(TAG, "Response from url: " + jsonStr);

            if (jsonStr != null) {
                // TENGO PROBLEMAS CON EL STRING LEIDO, ES MUY LARGO Y NO LO GUARDA ENTERO, DEJANDO EL FINAL MAL FORMADO
                // Solo me funciona con gson, con la librería json de android no me funciona, el bufferedreader y otros en java solo me leen 16384 caracteres. Bhttps://stackoverflow.com/questions/37680780/java-http-client-to-read-response-greater-than-16384-characters
                Gson gson = new GsonBuilder().setPrettyPrinting().create();

                // Necesario para formar un array del json https://futurestud.io/tutorials/gson-mapping-of-arrays-and-lists-of-objects
                medicionListType = new TypeToken<ArrayList<MedicionContaminacion>>(){}.getType();
                medicionList = gson.fromJson(jsonStr, medicionListType);

            } else {
                Log.e(TAG, "Couldn't get json from server.");
                runOnUiThread(new Runnable() { // Hilo para mostrar desde hilo secundario en hilo principal
                    @Override
                    public void run() {
                        Toast.makeText(getApplicationContext(),
                                "Couldn't get json from server. Check LogCat for possible errors!",
                                Toast.LENGTH_LONG)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            // CREAR UN MAKER PARA CADA UNO DE LOS DATOS LEIDOS DEL JSON
            if(medicionList!=null && medicionList.size()>0)
                dibujaMarkersContaminacion(medicionList);
        }

    }

    private void dibujaMarkersContaminacion(List<MedicionContaminacion> medicionList) {
        int color = Color.argb(50, 255, 0, 0);
       int i=0;
        if(mMap!=null) {
            for(MedicionContaminacion m : medicionList){
                if(m.getPpm()>600) {
                    LatLng latLng = new LatLng(m.getLatitud(), m.getLongitud());
                    if(!localizacionDibujadasContiene(latLng)) {
                        // System.out.println("Dibuja: "+i);
                        Circle circle = mMap.addCircle(new CircleOptions()
                                .center(latLng)
                                .radius(500)
                                //.strokeColor(Color.RED)
                                .strokeWidth(0)
                                .fillColor(color));
                        // circle.setStrokeWidth(0);
                        i++;
                    }
                }
            }
        }
    }

    private boolean localizacionDibujadasContiene(LatLng latLng) {
        int i=0;
        boolean enc=false;
        // System.out.println("DibujaSize: "+localizacionDibujadas.size());
        while(i<localizacionDibujadas.size() && !enc){
            if(localizacionDibujadas.get(i).latitude == latLng.latitude &&
                    localizacionDibujadas.get(i).longitude == latLng.longitude)
                enc=true;
            else {
                i++;
            }
        }

        if(!enc) // Si no está, la añadimos para que no se vuelva a dibujar
            localizacionDibujadas.add(latLng);

        return enc;
    }

    private void escribeFichero(String jsonStr) {
        File log = new File(Environment.getExternalStorageDirectory(), "nestor.txt");
        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(log.getAbsolutePath(), false));
            out.append(jsonStr);
            out.append(" : \n");
        } catch (Exception e) {
            Log.e(TAG, "Error opening Log.", e);
        }
        /*
        try
        {
            OutputStreamWriter fout=
                    new OutputStreamWriter(
                            openFileOutput("nestor.txt", Context.MODE_PRIVATE));

            fout.write(jsonStr);
            fout.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al escribir fichero a memoria interna. "+ex.getMessage());
        }
        */
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        // Finalizamos todas las tareas en segundo plano.
        if(jsonTask!=null)
            jsonTask.cancel(true);
        if(timer!=null)
            timer.cancel();
        if(task!=null)
            task.cancel();
        this.finish();
    }
}

