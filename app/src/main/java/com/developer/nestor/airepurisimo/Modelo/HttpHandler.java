package com.developer.nestor.airepurisimo.Modelo;

import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by Ravi Tamada on 01/09/16.
 * www.androidhive.info
 */
public class HttpHandler {

    private static final String TAG = HttpHandler.class.getSimpleName();

    public HttpHandler() {
    }

    public String makeServiceCall(String reqUrl) {
        String response = null;
        try {
            URL url = new URL(reqUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return response;
    }



    private String convertStreamToStringScanner(InputStream is) {
        // BufferedReader reader = new BufferedReader(new InputStreamReader(is)); // StandardCharsets.UTF_8));
        Scanner scanner = new Scanner(is, "UTF-8");
        StringBuilder sb = new StringBuilder();

        // sb.setLength(8192);
        String line;
        // char c;
        try {
            while (scanner.hasNextLine()) { // while ((c = (char)reader.read()) != -1) {
                // sb.append(c);
                // String s = scanner.next();
                line = scanner.nextLine();
                System.out.println(line);
                sb.append(line);// .append('\n');
            }
        } finally {
            try {
                is.close();
                scanner.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is), 30000);
        StringBuilder sb = new StringBuilder();

        // sb.setLength(8192);
        String line;
        // char c;
        try {
            while ((line = reader.readLine()) != null) { // while ((c = (char)reader.read()) != -1) {
                // sb.append(c);
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}