package com.developer.nestor.airepurisimo.Modelo;

import com.google.gson.annotations.SerializedName;

public class MedicionContaminacion {
    @SerializedName("_id")
    String _id;
    @SerializedName("fecha")
    String fecha;
    @SerializedName("hora")
    String hora;
    @SerializedName("ppm")
    double ppm;
    @SerializedName("latitud")
    double latitud;
    @SerializedName("longitud")
    double longitud;
    @SerializedName("temperatura")
    float temperatura;
    @SerializedName("humedad")
    int humedad;
    @SerializedName("__v")
    int __v;

    public MedicionContaminacion(){

    }

    public MedicionContaminacion(String _id, String fecha, String hora, double ppm, double latitud, double longitud, float temperatura, int humedad, int __v) {
        this._id = _id;
        this.fecha = fecha;
        this.hora = hora;
        this.ppm = ppm;
        this.latitud = latitud;
        this.longitud = longitud;
        this.temperatura = temperatura;
        this.humedad = humedad;
        this.__v = __v;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public double getPpm() {
        return ppm;
    }

    public void setPpm(double ppm) {
        this.ppm = ppm;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public float getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(float temperatura) {
        this.temperatura = temperatura;
    }

    public int getHumedad() {
        return humedad;
    }

    public void setHumedad(int humedad) {
        this.humedad = humedad;
    }
    @Override
    public String toString() {
        return "MedicionContaminacion{" +
                "_id='" + _id + '\'' +
                ", fecha='" + fecha + '\'' +
                ", hora='" + hora + '\'' +
                ", ppm=" + ppm +
                ", latitud=" + latitud +
                ", longitud=" + longitud +
                ", temperatura=" + temperatura +
                ", humedad=" + humedad +
                ", __v=" + __v +
                '}';
    }
}
